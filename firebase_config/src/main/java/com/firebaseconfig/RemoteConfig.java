package com.firebaseconfig;

import android.app.Activity;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;



public class RemoteConfig {

    private static final String SUCCESS = "success",FAILURE = "failure";

    public interface RemoteCallback {
        void onValidUser();
        void onInvalidUser(String status);
        void onError(String message);
    }

    private final Activity activity;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private RemoteCallback remoteCallback;

    private RemoteConfig(Activity activity) {
        this.activity = activity;
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600L)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
    }

    public static RemoteConfig newInstance(Activity activity) {
        return new RemoteConfig(activity);
    }

    public void fetch(final RemoteCallback remoteCallback) {
        this.remoteCallback = remoteCallback;
        // cacheExpirationSeconds is set to cacheExpiration here, indicating the next fetch request
        // will use fetch data from the Remote Config service, rather than cached parameter values,
        // if cached parameter values are more than cacheExpiration seconds old.
        // See Best Practices in the README for more information.
//        long cacheExpiration = 43200;// 12 hours in seconds.
        long cacheExpiration;
        if (BuildConfig.DEBUG) {
            cacheExpiration = 0;
        } else {
            cacheExpiration = mFirebaseRemoteConfig.getInfo().getConfigSettings().getMinimumFetchIntervalInSeconds();
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String status;
                        if (task.isSuccessful()) {
                            status = SUCCESS;
                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activate();
                        } else {
                            status = FAILURE;
                        }
                        displayWelcomeMessage(status);
                    }
                });
    }

    private void displayWelcomeMessage(String status) {
        String isAppLive = mFirebaseRemoteConfig.getString("isAppLive");
        String message = mFirebaseRemoteConfig.getString("message");
        if(remoteCallback !=null){
            if(status.equals(SUCCESS)) {
                if(!TextUtils.isEmpty(isAppLive) && isAppLive.equals("true")) {
                    remoteCallback.onValidUser();
                }else{
                    remoteCallback.onInvalidUser(message);
                }
            }else{
                remoteCallback.onError(status);
            }
        }
    }

}
