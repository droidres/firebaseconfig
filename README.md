# FirebaseConfig 
  
## Setup Project

Add this to your project build.gradle
Project-level build.gradle (<project>/build.gradle):

``` gradle
buildscript { 
    repositories {
        google()
        jcenter()
        maven {
            url 'https://maven.fabric.io/public'
        }
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:3.2.1'

        classpath 'com.google.gms:google-services:4.2.0' 
        classpath 'io.fabric.tools:gradle:1.26.1'
 
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven {
            url 'https://maven.google.com/'
        }
        maven { url 'https://jitpack.io' } 
    }
}
```




Add this to your project build.gradle
Module-level build.gradle (<module>/build.gradle): 

[![](https://jitpack.io/v/org.bitbucket.droidres/firebaseconfig.svg)](https://jitpack.io/#org.bitbucket.droidres/firebaseconfig)
```gradle 
apply plugin: 'io.fabric'

dependencies {
    implementation 'org.bitbucket.droidres:firebaseconfig:x.y'

    implementation 'com.crashlytics.sdk.android:crashlytics:2.9.8'
}

// ADD THIS AT THE BOTTOM
apply plugin: 'com.google.gms.google-services'
```




## Setup Firebase
```gradle
Add Remote Config Parameter
1.  Key    : isAppLive
    Value  : true
2.  Key    : message
    Value  : Trial period is over. Please contact to IT Team.
     
Firebase Crashlytics
Before you begin
To get started, you need a Firebase app with Firebase Crashlytics enabled:
```
  
 
 
 
#### Usage method
```java 
      RemoteConfig.newInstance(this)
                .fetch(new RemoteConfig.RemoteCallback() {
                    @Override
                    public void onValidUser() {

                    }

                    @Override
                    public void onInvalidUser(String message) {

                    }

                    @Override
                    public void onError(String message) {

                    }
                });
```



#### Test your Firebase Crashlytics implementation
```java 
      Crashlytics.getInstance().crash(); 
```



#### Useful Links:
1. https://firebase.google.com/docs/android/setup
2. https://firebase.google.com/docs/crashlytics/get-started